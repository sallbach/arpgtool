**free
/IF NOT DEFINED (AR_HASH__)

/DEFINE AR_HASH__
/INCLUDE QCPYLESRC,ARCTYPES

//---------------------------------------------------------------*
// Constants
//---------------------------------------------------------------*
dcl-c HASH_MD5 1;
dcl-c HASH_SHA1 2;
dcl-c HASH_SHA256 3;
dcl-c HASH_SHA384 4;
dcl-c HASH_SHA512 5;

dcl-c HASH_CSP_ANY '0';
dcl-c HASH_CSP_SOFTWARE '1';
dcl-c HASH_CSP_HARDWARE '2';

dcl-c HASH_OPT_CONTINUE 0;
dcl-c HASH_OPT_FINAL 1;

//---------------------------------------------------------------*
// Qc3CalculateHash - Calculate Hash
//---------------------------------------------------------------*
dcl-pr Qc3CalculateHash int(10) extproc(*dclcase);
  data pointer value options(*string);
  datasize int(10) const;
  dataformat char(8) const;
  algorithmDescription char(65535) options(*varsize);
  algorithmFormat char(8) const;
  CryptographicServiceProvider char(1) const;
  CryptographicDeviceName char(10) const options(*omit);
  hash char(65535) options(*varsize);
  uSec char(65535) options(*varsize) noopt;
end-pr;

/ENDIF
